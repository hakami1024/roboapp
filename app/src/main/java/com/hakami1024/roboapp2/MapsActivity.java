package com.hakami1024.roboapp2;

import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Log.d("MapsActivity", "Current zoom: "+mMap.getCameraPosition().zoom);
            }
        });

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);

//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        LatLngBounds b = mMap.getProjection().getVisibleRegion().latLngBounds;
        //TODO: fix distance
        double size = distance(b.northeast, b.southwest);
        size = 500;

        Log.d("MapsActivity", "size: "+size);


        new AsyncTask<Double, Void, ArrayList<ArrayList<LatLng>>>(){

            @Override
            protected ArrayList<ArrayList<LatLng>> doInBackground(Double... size) {
                ArrayList<LatLng> markers = new ArrayList<>();

                for(int i=1; i<=1; i++){
                    try {
                        JSONArray a = new JSONArray(loadJSONFromAsset("data-"+i));
                        jsonToLatLng(a, markers);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                Log.d("MapsActivity", "MarkersCount: "+markers.size());

                DistanceMetric<LatLng> metric = new DistanceMetric<LatLng>() {
                    @Override
                    public double calculateDistance(LatLng val1, LatLng val2) throws DBSCANClusteringException {
                        return distance(val1, val2);
                    }
                };

                ArrayList<ArrayList<LatLng>> results = null;
                try {
                    Log.d("MapsActivity", "clusterer with size: "+size[0]/5);

                    DBSCANClusterer<LatLng> clusterer = new DBSCANClusterer<>(markers, 2, size[0]/5.0, metric);
                    long start = System.currentTimeMillis();
                    Log.d("MapsActivity", "clusterization started");
                    results = clusterer.performClustering();
                    long elapsedTime = System.currentTimeMillis() - start;
                    Log.d("MapsActivity", "clusterization finished, seconds: "+elapsedTime/1000F);
                } catch (DBSCANClusteringException e) {
                    e.printStackTrace();

                }

                return results;
            }

            @Override
            protected void onPostExecute(ArrayList<ArrayList<LatLng>> latLngs) {
                Log.d("MapsActivity", "clusters count: "+latLngs.size());

                for(ArrayList<LatLng> cluster: latLngs){
                    Log.d("MapsActivity", "cluster size: "+cluster.size());

                    double meanLat=0, meanLon=0;

                    for(LatLng p: cluster){
                        meanLat += p.latitude;
                        meanLon += p.longitude;
                    }

                    meanLat /= cluster.size();
                    meanLon /= cluster.size();

                    mMap.addMarker(new MarkerOptions().position(new LatLng(meanLat, meanLon)));

                }
//                for(LatLng latLng: latLngs){
//                    mMap.addMarker(new MarkerOptions().position(latLng));
//                }

//                DistanceMetric<LatLng> metric = new DistanceMetric<LatLng>() {
//                    @Override
//                    public double calculateDistance(LatLng val1, LatLng val2) throws DBSCANClusteringException {
//                        return distance(val1, val2);
//                    }
//                };
//
//                LatLngBounds b = mMap.getProjection().getVisibleRegion().latLngBounds;
//                double size = distance(b.northeast, b.southwest);
//
//                try {
//                    DBSCANClusterer<LatLng> clusterer = new DBSCANClusterer<>(latLngs, 1, size/5, metric);
//                    ArrayList<ArrayList<LatLng>> results = clusterer.performClustering();
//                } catch (DBSCANClusteringException e) {
//                    e.printStackTrace();
//
//                }
            }
        }.execute(size);

//        ArrayList<LatLng> markers = new ArrayList<>();
//        try {
//            JSONArray a1 = new JSONArray(loadJSONFromAsset("data-1"));
//            JSONArray a2 = new JSONArray(loadJSONFromAsset("data-2"));
//            JSONArray a3 = new JSONArray(loadJSONFromAsset("data-3"));
//
//            jsonToLatLng(a1, markers);
//            jsonToLatLng(a2, markers);
//            jsonToLatLng(a3, markers);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        for(LatLng latLng: markers){
//            mMap.addMarker(new MarkerOptions().position(latLng));
//        }
    }

    private double distance(LatLng val1, LatLng val2) {
        float[] results = new float[1];
        Location.distanceBetween(
                val1.latitude,
                val1.longitude,
                val2.latitude,
                val2.longitude,
                results);
        return results[0];
    }

    private void jsonToLatLng(JSONArray jsonMarkers, ArrayList<LatLng> markers) throws JSONException {
        int size = jsonMarkers.length();
        if(size  > 1000){
            size = 1000;
        }
        for(int i=0; i<size; i++){
            JSONObject obj = jsonMarkers.getJSONObject(i);

            markers.add(new LatLng(obj.getDouble("lat"), obj.getDouble("lon")));
        }
    }

    public String loadJSONFromAsset(String name) {
        String json;
        try {

            InputStream is = getAssets().open(name+".json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }
}
